var app = angular.module('app', ['ngRoute']);

app.controller('homeCtrl', function($scope, $rootScope){
    $rootScope.loading = false;
    $scope.username = 'Admin';
});

app.controller('userCtrl', function($scope, $rootScope, $http){
    var url = '/users';
    $rootScope.loading = true;
    $scope.users = [];
    $http.get(url).then(function(res){
        if (res.data){
            $scope.users = res.data;
            $rootScope.loading = false;
        } else {
            alert('No data available.');
            console.log(res);
        }
    }, function(err){
        alert('Error retrieving data.');
        console.log(err);
    });

    $scope.editUser = function(id){
        $http.get(url+'/'+id).then(function(res){
            if (res.data){
                $scope.newUser = res.data;
            }
        }, function(err){
            alert('Cannot find user');
            console.log(err);
        });
    }

    $scope.deleteUser = function(id){
        $http.delete(url+'/'+id).then(function(res){
            if (res.data.id){
                $scope.users.splice($scope.getIndexById(res.data.id), 1);
            }
        }, function(err){
            alert('Error when deleting.');
            console.log(err);
        });
    }
    $scope.addUser = function(){
        if (typeof($scope.newUser)=='undefined' || angular.equals($scope.newUser, {})){
            alert('Please fill in all required fields.');
            return;
        }
        if($scope.newUser.id){
            // Existing user
            $http.put(url, JSON.stringify($scope.newUser)).then(function(res){
                $scope.users[$scope.getIndexById(res.data.id)] = res.data;
            }, function(err){
                alert('Error saving data.');
                console.log(err);
            });

        } else {
            // Adding new user
            $http.post(url, JSON.stringify($scope.newUser)).then(function(res){
                $scope.users.push(res.data);
            }, function(err){
                alert('Cannot save object');
                console.log(err);
            });
        }
        this.resetUser();
    }

    $scope.resetUser = function(){
        $scope.newUser = {};
    }

    $scope.getIndexById = function(id){
        for (var i = 0; i < $scope.users.length; i++){
            if (id == $scope.users[i].id){
                return i;
            }
        };
        return -1;
    }
});

app.controller('surveyCtrl', function($scope, $rootScope, $http){
    var url = '/survey';
    $rootScope.loading = true;
    $scope.surveys = [];

    $http.get(url).then(function(res){
        if( res.data ){
            $scope.surveys = res.data;
        }
        $rootScope.loading = false;
    }, function(err){
        alert('No data available.');
        console.log(err);
    });

    $scope.editSurvey = function(id){
        $http.get(url+'/'+id).then(function(res){
            if (res.data){
                $scope.surveyInstance = res.data;
            }
        }, function(err){
            alert('Cannot find survey');
            console.log(err);
        });
    }

    $scope.deleteSurvey = function(id){
        $http.delete(url+'/'+id).then(function(res){
            if (res.data.id){
                $scope.surveys.splice($scope.getIndexById(res.data.id), 1);
            }
        }, function(err){
            alert('Error when deleting.');
            console.log(err);
        });
    }

    $scope.addSurvey = function(){
        if (typeof($scope.surveyInstance)=='undefined' || angular.equals($scope.surveyInstance, {})){
            alert('Please fill in all required fields.');
            return;
        }
        if($scope.surveyInstance.id){
            // Existing survey
            $http.put(url, JSON.stringify($scope.surveyInstance)).then(function(res){
                $scope.surveys[$scope.getIndexById(res.data.id)] = res.data;
            }, function(err){
                alert('Error saving data.');
                console.log(err);
            });

        } else {
            // Adding new survey
            $http.post(url, JSON.stringify($scope.surveyInstance)).then(function(res){
                console.log(res);
                $scope.surveys.push(res.data);
            }, function(err){
                alert('Cannot save object');
                console.log(err);
            });
        }
        this.resetSurvey();
    }

    $scope.getIndexById = function(id){
        for (var i = 0; i < $scope.surveys.length; i++){
            if (id == $scope.surveys[i].id){
                return i;
            }
        };
        return -1;
    }

    $scope.resetSurvey = function(){
        $scope.surveyInstance = {};
    }


});

// Routes
app.config(function($routeProvider) {
  $routeProvider
  .when('/home', {
    //templateUrl: 'partials/home.html',
    template: '<p>Welcome {{ username }}</p>',
    controller: 'homeCtrl'
  })
  .when('/users', {
    templateUrl: '/partials/users.html',
    controller: 'userCtrl'
  })
  .when('/survey', {
    templateUrl: '/partials/survey.html',
    controller: 'surveyCtrl'
  })
  .otherwise({redirectTo: '/'});
});