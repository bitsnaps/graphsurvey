import ratpack.groovy.Groovy
import ratpack.groovy.Groovy
import static ratpack.groovy.Groovy.groovyTemplate
import ratpack.groovy.template.TextTemplateModule

import com.corposense.SessionProvider
import com.corposense.domain.*
import com.corposense.handlers.*
import static ratpack.jackson.Jackson.json

Groovy.ratpack {

    bindings {
        module(TextTemplateModule)
    }
    handlers {
        get {
//            render file('templates/index.html')
            render(groovyTemplate([title:'GraphSurvey'], 'index.html'))
        }
        prefix('users'){
            def userHandler = new UserHandler()
            path( userHandler )
            path('/:id', userHandler)
        }
        prefix('survey'){
            def surveyHandler = new SurveyHandler()
            path( surveyHandler )
            path('/:id', surveyHandler)
        }


        /*get {
            String firstName = ''
            def session = SessionProvider.instance.session
            //def person = session.load(Person, 1l)
            //def ibrahim = new Person(name:'Ibrahim', 'born':1984)
    //      session.save(ibrahim)
            def persons = session.loadAll(Person)
            println("There are: ${persons.size()} person(s):")
            firstName = persons.last().name
//        persons.each {
//            println("Person: ${it.id}: ${it.name} ${it.born?'was born on: '+it.born:'wasn\'t born yet!'}")
//        }


//            render(groovyTemplate([user:'firstName'], 'index.html'))
        }*/

        files {
            dir('public')
        }


    }
}