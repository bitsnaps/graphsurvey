package com.corposense

import org.neo4j.ogm.config.ClasspathConfigurationSource
import org.neo4j.ogm.config.Configuration
import org.neo4j.ogm.session.Session
import org.neo4j.ogm.session.SessionFactory

@Singleton
class SessionProvider implements AutoCloseable {

    private static Configuration = new Configuration.Builder(new ClasspathConfigurationSource("ogm.properties"))
            .build()

    private static SessionFactory sessionFactory = new SessionFactory(Configuration, "com.corposense.domain")

    Session getSession(){
        sessionFactory.openSession()
    }

    @Override
    void close() throws IOException {
        sessionFactory.close()
    }

}