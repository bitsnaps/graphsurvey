package com.corposense.domain

import groovy.transform.ToString
import org.neo4j.ogm.annotation.GeneratedValue
import org.neo4j.ogm.annotation.Id
import org.neo4j.ogm.annotation.NodeEntity
import org.neo4j.ogm.annotation.Relationship
import org.neo4j.ogm.annotation.typeconversion.DateLong

@ToString
@NodeEntity
class Person {

  @Id @GeneratedValue
  Long id
  String name
  Integer born


//  @Relationship('PARTICIPATE')
//  Survey survey
  
}

/*


@NodeEntity
class Question {

  @Id @GeneratedValue
  Long id
  String name
  String text
  boolean answerRequired
  QuestionType type
  int surveyOrder
//    GroupQuestion group

  boolean isChoice() {
    type in [SingleChoice, MultipleChoice]
  }

  boolean hasGroup() {
    (type in [SingleChoice, MultipleChoice])// && group != null)
  }

  String toString() { "Question (${name})" }
}

enum QuestionType {
  FreeText, Numeric, Decimal, Email, Tel, Date, YesNo, SingleChoice, MultipleChoice, GPS
}*/