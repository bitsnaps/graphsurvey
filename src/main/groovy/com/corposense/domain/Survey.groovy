package com.corposense.domain

import groovy.transform.ToString
import org.neo4j.ogm.annotation.GeneratedValue
import org.neo4j.ogm.annotation.Id
import org.neo4j.ogm.annotation.NodeEntity
import org.neo4j.ogm.annotation.typeconversion.DateLong
import org.neo4j.ogm.annotation.typeconversion.DateString;

@ToString
@NodeEntity
class Survey {

  @Id @GeneratedValue
  Long id
  String name
  String description
  String welcomeMessage
  String exitMessage
  @DateString("dd-MM-yyyy")
  Date startDate
  @DateString("dd-MM-yyyy")
  Date publicationDate
  @DateString("dd-MM-yyyy")
  Date expirationDate
//  @Relationship('CONTAINS_MANY')//, direction = Relationship.INCOMING)
//  Set<Question> questions
//  String questions

//    String toString() { "Survey (${name})" }

//  String toJson(){
//    String result = """{
//        "survey_properties": {
//            "intro_message": "${this.welcomeMessage}",
//            "end_message": "${this.exitMessage}",
//            "skip_intro": "${this.welcomeMessage.toString().isEmpty()}"
//        }, "questions": ${this.questions}
//        }"""
////        log("result: ${result}")
//    result
//  }
}