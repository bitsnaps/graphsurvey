import com.corposense.SessionProvider
import com.corposense.domain.Person
import groovy.util.logging.Slf4j
import org.neo4j.ogm.cypher.query.Pagination
import org.neo4j.ogm.cypher.query.SortOrder
import org.neo4j.ogm.session.Session
import ratpack.func.Action
import ratpack.handling.ByMethodSpec
import ratpack.handling.Context
import ratpack.handling.Handler
import ratpack.jackson.Jackson

/**
 * Created by Master on 26/07/2018.
 */
@Slf4j
class UserHandler implements Handler {

    private Session session = SessionProvider.instance.session

    @Override
    void handle(Context ctx) throws Exception {
        def res = ctx.response
        ctx.byMethod({ ByMethodSpec method ->
            method.options({
                res.headers.set('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, DELETE, PUT')
                res.headers.set('content-type', 'application/json')
                res.send()
            })
            .get({
                if (ctx.pathTokens['id']) {
                    ctx.render(Jackson.json(session.load(Person, ctx.pathTokens['id'].toLong())))
                } else {
                    def persons = session.loadAll(Person, new SortOrder().add("name"),  new Pagination(0, 5))
                    log.info("Loading: ${persons.size()} person(s)")
                    ctx.render(Jackson.json(persons))
                }
            })
            .post({
                ctx.parse(Person).then({ Person person ->
                    log.info("Saving: ${person}")
                    try {
                        session.save(person)
                        def p = session.load(Person, person.id)
                        ctx.render(Jackson.json(p))
                    } catch (Exception e){
                        res.status(403).send(e.message)
                    }
                })
            })
            .put({
                ctx.parse(Jackson.fromJson(Person)).then({ Person person ->
                    log.info("Person: ${person}")
                    session.save( person )
                    ctx.render(Jackson.json(person))
                })
            })
            .delete({
                def p = session.load(Person, ctx.pathTokens.id.toLong())
                if (p){
                    session.delete(p)
                    log.info("Person deleted: ${ctx.pathTokens.id}")
                    ctx.render(Jackson.json([id:p.id]))
                } else {
                    res.status(404).send('User could not be found.')
                }
            })
        } as Action<ByMethodSpec>)

    }
}