package com.corposense.handlers

import com.corposense.SessionProvider
import com.corposense.domain.Survey
import groovy.util.logging.Slf4j
import org.neo4j.ogm.cypher.query.Pagination
import org.neo4j.ogm.cypher.query.SortOrder
import org.neo4j.ogm.session.Session
import ratpack.func.Action
import ratpack.handling.ByMethodSpec
import ratpack.handling.Context
import ratpack.handling.Handler
import ratpack.jackson.Jackson

/**
 * Created by Master on 27/07/2018.
 */
@Slf4j
class SurveyHandler implements Handler {

    private Session session = SessionProvider.instance.session

    @Override
    void handle(Context ctx) throws Exception {

        def res = ctx.response
        ctx.byMethod({ ByMethodSpec method ->
            method.options({
                res.headers.set('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, DELETE, PUT')
                res.headers.set('content-type', 'application/json')
                res.send()
            }).get({
                if (ctx.pathTokens['id']) {
                    ctx.render(Jackson.json(session.load(Survey, ctx.pathTokens['id'].toLong())))
                } else {
                    def surveys = session.loadAll(Survey, new SortOrder().add("name"), new Pagination(0, 5))
                    log.info("Loading ${surveys.size()} survey(s)")
                    ctx.render(Jackson.json(surveys))
                }
            }).post({
                ctx.parse(Survey).then({ Survey survey ->
                    try {
                        log.info("Saving: ${survey}")
                        session.save(survey)
                        def s = session.load(Survey, survey.id)
                        ctx.render(Jackson.json(s))
                    } catch (Exception e){
                        res.status(403).send(e.message)
                    }
                })
            }).put({
                ctx.parse(Jackson.fromJson(Survey)).then({ Survey survey ->
                    log.info("Survey: ${survey}")
                    session.save( survey )
                    ctx.render(Jackson.json(survey))
                })
            }).delete({
                def s = session.load(Survey, ctx.pathTokens.id.toLong())
                if (s){
                    session.delete(s)
                    log.info("Survey deleted: ${ctx.pathTokens.id}")
                    ctx.render(Jackson.json([id:s.id]))
                } else {
                    res.status(404).send('Survey could not be found.')
                }
            })
        } as Action<ByMethodSpec>)

    }
}
